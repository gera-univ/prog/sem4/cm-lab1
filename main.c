#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "prime-checker.h"

int
main(int argc, char **argv) {
    long exponent = 100;
    long precision = 100;
    int verbosity = 0;
    int is_waiting_for_exponent = 0;
    int is_waiting_for_precision = 0;
    for (int i = 1; i < argc; ++i) {
        if (is_waiting_for_exponent) {
            exponent = strtol(argv[i], NULL, 10);
            is_waiting_for_exponent = 0;
        }
        if (is_waiting_for_precision) {
            precision = strtol(argv[i], NULL, 10);
            is_waiting_for_precision = 0;
        }
        if (strcmp("-h", argv[i]) == 0 || strcmp("--help", argv[i]) == 0) {
            printf("usage: %s [--exponent exp] [--precision prec] [--verbose] [--help]\n"
                   "       %s [-e exp] [-p prec] [-v] [-h]\n", argv[0], argv[0]);
            return 0;
        }
        if ((strcmp("--exponent", argv[i]) == 0) || (strcmp("-e", argv[i]) == 0) || (strcmp("-ve", argv[i]) == 0)) {
            is_waiting_for_exponent = 1;
        }
        if (strcmp("--precision", argv[i]) == 0 || strcmp("-p", argv[i]) == 0 || strcmp("-vp", argv[i]) == 0) {
            is_waiting_for_precision = 1;
        }

        if (strcmp("--verbose", argv[i]) == 0 || strcmp("-v", argv[i]) == 0 || strcmp("-vp", argv[i]) == 0 ||
            strcmp("-ve", argv[i]) == 0) {
            verbosity = 1;
        }
    }

    z_t random_max, num;
    jmp_buf env;
    char *buf;

    if (setjmp(env))
        return zperror(argv[0]), 1;

    zsetup(env);

    // random max имеет значение 2^exponent
    zinit(random_max);
    zsetu(random_max, 1);
    zlsh(random_max, random_max, exponent);

    zinit(num);
    do {
        zrand(num, DEFAULT_RANDOM, UNIFORM, random_max);
        if (verbosity > 0) {
            printf("checking %s\n", buf = zstr(num, NULL, 0));
            free(buf);
        }
    } while (!isprime(num, precision));

    printf("%s\n", buf = zstr(num, NULL, 0));
    free(buf);

    zfree(random_max);
    zfree(num);
    zunsetup();
    return 0;
}
