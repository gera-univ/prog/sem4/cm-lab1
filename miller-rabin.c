#include "prime-checker.h"

int
isprime(z_t n, long precision)
{
    z_t  a, r, v, n_minus_1, n_minus_2, gcd, v_mod_n, tmp;
    zinit(a);
    zinit(r);
    zinit(v);
    zinit(n_minus_1);
    zinit(n_minus_2);
    zinit(gcd);
    zinit(v_mod_n);
    zinit(tmp);

    zsetu(tmp, 2);
    zsub(n_minus_2, n, tmp);

    zsetu(tmp,1);
    zsub(n_minus_1, n, tmp);
    size_t s = zlsb(n_minus_1);
    zrsh(r, n_minus_1, s);

    int result = 0;

    for (long j = 0; j < precision; ++j) {
        // генерируем случайное число от 1 до n-1
        zrand(a, DEFAULT_RANDOM, UNIFORM, n_minus_2);
        zsetu(tmp, 1);
        zadd(a, a, tmp);

        zgcd(gcd, a, n);
        if (zcmpu(gcd, 1)) {
            break;
        }

        zmodpow(v, a, r, n);
        if (zcmpu(v, 1) == 0) {
            result = 1;
            break;
        }

        for (size_t i = 0; i < s; ++i) {
            zmod(v_mod_n, v, n);
            if (zcmpi(v_mod_n, -1) == 0) {
                result = 1;
                goto end;
            }
            zmodsqr(v, v, n);
        }
    }

    end:
    zfree(a);
    zfree(r);
    zfree(v);
    zfree(n_minus_1);
    zfree(n_minus_2);
    zfree(gcd);
    zfree(v_mod_n);
    zfree(tmp);

    return result;
}
