#include "prime-checker.h"

int
isprime(z_t n, long precision)
{
    if (zcmpi(n, 0) <= 0) {
        return 0;
    }
    if (zcmpu(n, 3) <= 0) {
        return 1;
    }
    z_t a, n_minus_1, n_minus_2, gcd, z_power_n_minus_1_mod_n, tmp;
    zinit(a);
    zinit(n_minus_1);
    zinit(n_minus_2);
    zinit(gcd);
    zinit(z_power_n_minus_1_mod_n);
    zinit(tmp);

    zsetu(tmp, 1);
    zsub(n_minus_1, n, tmp);
    zsetu(tmp, 2);
    zsub(n_minus_2, n, tmp);

    int result = 1;

    for (long i = 0; i < precision; ++i) {
        // генерируем случайное число от 1 до n-1
        zrand(a, DEFAULT_RANDOM, UNIFORM, n_minus_2);
        zsetu(tmp, 1);
        zadd(a, a, tmp);

        zgcd(gcd, a, n);
        if (zcmpu(gcd, 1)) {
            result = 0;
            break;
        }

        zmodpow(z_power_n_minus_1_mod_n, a, n_minus_1, n);
        if (zcmpu(z_power_n_minus_1_mod_n, 1)) {
            result = 0;
            break;
        }
    }

    zfree(a);
    zfree(n_minus_1);
    zfree(n_minus_2);
    zfree(gcd);
    zfree(z_power_n_minus_1_mod_n);
    zfree(tmp);

    return result;
}